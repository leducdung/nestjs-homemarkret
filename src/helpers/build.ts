import { Types } from 'mongoose';

const buildArrayObjectIds = (data: string | string[]) => {
  if (!data) {
    return;
  }

  if (!Array.isArray(data)) {
    return Types.ObjectId(data);
  }

  return data.map((each) => Types.ObjectId(each));
};

export const buildFindingQuery = ({
  query,
  fieldNeedToUseRegex = [],
  convertToObjectIdFields = [],
}) => {
  const {
    sortBy = '_id',
    limit,
    cursor,
    sortDirection,
    ...findingQuery
  } = query;
  const validDirection: number = sortDirection === 'ASC' ? 1 : -1;
  const hasPage = !!limit;
  const sortingCondition = { [sortBy]: validDirection };

  for (const key in findingQuery) {
    if (!key) {
      continue;
    }

    const shouldConvertToObjectId = convertToObjectIdFields.includes(key);

    if (shouldConvertToObjectId) {
      findingQuery[key] = buildArrayObjectIds(findingQuery[key]);
    }

    if (fieldNeedToUseRegex.includes(key)) {
      findingQuery[key] = { $regex: findingQuery[key], $options: 'i' };

      continue;
    }

    if (!Array.isArray(findingQuery[key])) {
      continue;
    }

    findingQuery[key] = { $in: findingQuery[key] };
  }

  const findAllQuery = { ...findingQuery };

  if (!limit) {
    return {
      findingQuery,
      findAllQuery,
      sortingCondition,
      hasPage,
    };
  }

  if (!cursor) {
    return {
      sortingCondition,
      findingQuery,
      findAllQuery,
      hasPage,
    };
  }

  const condition = validDirection === 1 ? '$gt' : '$lt';

  findingQuery[sortBy] = { [condition]: cursor };

  if (convertToObjectIdFields.includes(sortBy)) {
    findingQuery[sortBy] = { [condition]: Types.ObjectId(cursor) };
  }

  return {
    sortingCondition,
    findingQuery,
    findAllQuery,
    hasPage,
  };
};

export const generateChartData = (data) => {
  if (!data || !data.length) {
      return []
  }

  data = data.map(each => {
      return {
          date: new Date(each._id),
          count: each.count,
      }
  })

  data = data.sort((a, b) => {
      return a.date.getTime() > b.date.getTime() ? -1 : 1
  })

  const dateNow = new Date()
  const timeNow = dateNow.getTime()+(1000 * 60 * 60 * 24);

  const date111 = new Date(timeNow)
  const date222 =  date111.toISOString()
  const dateNow111= `${date222.slice(0,10)}T00:00:00.000Z`

  const maxDate =  new Date(dateNow111)
  const minDate = new Date('2021-04-13T00:00:00.000Z')

  const rangeTime = maxDate.getTime() - minDate.getTime()


  const rangeDate = Math.floor(rangeTime / (1000 * 60 * 60 * 24))



  for (let i = 1; i < rangeDate; i++) {
      const tempDate = new Date(minDate.toString())

      tempDate.setDate(tempDate.getDate() + i)
      const findExistDateIndex = data.findIndex(element => {
          return element.date.toString() === tempDate.toString()
      })
      if (findExistDateIndex === -1) {
          data.push({
              date: tempDate,
              count: 0,
          })
      }
  }

  data = data.sort((a, b) => {
      return a.date.getTime() > b.date.getTime() ? 1 : -1
  })

  return data
}

export const formatCash = (str) => {
  return str.split('').reverse().reduce((prev, next, index) => {
    return ((index % 3) ? next : (next + ',')) + prev
  })
}
