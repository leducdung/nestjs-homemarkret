export enum UserAccessStatus {
  NOSTATUS = 'NOSTATUS',
  PENDING = 'PENDING',
  ACCEPTED = 'ACCEPTED',
  REJECTED = 'REJECTED',
}

export enum UserStatus {
  UNLOCK = 'UNLOCK',
  LOCK = 'LOCK',
}

export enum UserRole {
  BASIC = 'BASIC',
  ADMIN = 'ADMIN',
  EMPLOYEE = 'EMPLOYEE',
  STOREOWNER = 'STOREOWNER',
}

export enum SortDirection {
  asc = 'ASC',
  desc = 'DESC',
}

export enum Gender {
  Male = 'Male',
  Female = 'Female',
}

export enum EventNotification {
  ORDERING = 'ORDERING',
  ORDERED = 'ORDERED',
}

export enum HistoryOderStoresStatus {
  COMPOSING = 'COMPOSING',
  FINISHED_COMPOSING = 'FINISHED_COMPOSING',
}

export enum HistoryOderUserStatus {
  COMPOSING = 'COMPOSING',
  FINISHED_COMPOSING = 'FINISHED_COMPOSING',
}

export enum statusProduct {
  OUT_OF_STOCK = 'OUT_OF_STOCK',
  STOCKING = 'STOCKING',
}

export enum productsTag {
  FRUIT = 'FRUIT', // gia cầm
  DRINKS = 'DRINKS', // thịt bò thịt heo
  MEAT = 'MEAT',
  SEA_FOOD = 'SEA_FOOD', // gia vị
  MILK_AND_AGE = 'MILK_AND_AGE',
  BREAD = 'BREAD',
  FROZREN = 'FROZREN',
  ORGANIC = 'ORGANIC'
}

export enum PaymentStatus {
  DIRECT_PAYMENT = 'DIRECT_PAYMENT',
  PENDING = 'PENDING',
  PAYMENTED = 'PAYMENTED',
  SUCCESS = 'SUCCESS',
  ERROR = 'ERROR'
}

export enum GiaoHangStatus {
  GIAO_HANG = 'GIAO_HANG',
  TU_LAY_HANG = 'TU_LAY_HANG',
}

export enum OrderStatus {
  DONE = 'DONE',
  DELIVEING = 'DELIVEING',
  PREPARING_ORDER = 'PREPARING_ORDER',
  FINISH_PACKING = 'FINISH_PACKING',
}
export const sortDirection = [SortDirection.asc, SortDirection.desc];
