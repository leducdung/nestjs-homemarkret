import { Module, forwardRef } from '@nestjs/common';
import { ProductsService } from './products.service';
import { MongooseModule } from '@nestjs/mongoose';
import { ProductsSchema } from './model/products.schema';
import { AuthModule } from '../../auth/auth.module';
import { databaseModule } from '../../database/database.module';
import { ProductsController } from './products.controller';
import { ProductsCombinedService } from './products.combined.service';
import { UsersModule } from '../users/users.module';
import { FirebaseModule } from "../fireBase/firebase.module";
import { PaymentModule } from "../payment/payment.module";

export const ProductModel = MongooseModule.forFeature([
  {
    name: 'Products',
    schema: ProductsSchema,
  },
]);

@Module({
  imports: [
    forwardRef(() => AuthModule),
    forwardRef(() => databaseModule),
    forwardRef(() => ProductModel),
    forwardRef(() => UsersModule),
    forwardRef(() => FirebaseModule),
  ],
  controllers: [ProductsController],
  providers: [ProductsService,
     ProductsCombinedService,
    ],
  exports: [ProductsService, ProductsCombinedService],
})
export class ProductsModule {}
