import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Types } from 'mongoose';
import { sortDirection, SortDirection } from '../../../constains/common';
import {
  UserStatus,
  statusProduct,
  productsTag,
} from '../../../constains/common';
export enum SortBy {
  id = '_id',
  title = 'title',
  name = 'name',
  amount = 'amount',
  price = 'price',
  quantitySold='quantitySold',
  percentDiscount='percentDiscount',
  createdAt = 'createdAt',
  updatedAt = 'updatedAt',
}
export class CreateProductDto {
  @ApiPropertyOptional()
  readonly name?: string;

  @ApiPropertyOptional()
  readonly description?: string;

  @ApiPropertyOptional()
  readonly photos?: string;

  @ApiPropertyOptional()
  readonly price?: number;

  @ApiPropertyOptional()
  readonly origin?: string;

  @ApiPropertyOptional()
  readonly nextWeight?: string;

  @ApiPropertyOptional({ enum: statusProduct })
  readonly status?: string;

  @ApiPropertyOptional()
  readonly tradeMark?: string;

  @ApiPropertyOptional({ enum: productsTag })
  readonly tag?: string;

  @ApiPropertyOptional()
  readonly paymentCode?: string;

  @ApiPropertyOptional()
  readonly inCard?: boolean;

  @ApiPropertyOptional()
  readonly percentDiscount?: number;

  @ApiPropertyOptional()
  readonly quantitySold?: number;

  @ApiPropertyOptional()
  readonly calories?: string;

  @ApiPropertyOptional({ enum: UserStatus })
  readonly statusAccount?: string;

  @ApiPropertyOptional()
  readonly amount?: number;
}

export class DataUpdateProductDto {
  @ApiPropertyOptional()
  readonly name?: string;

  @ApiPropertyOptional()
  readonly description?: string;

  @ApiPropertyOptional()
  readonly photos?: string;

  @ApiPropertyOptional()
  readonly price?: number;

  @ApiPropertyOptional()
  readonly origin?: string;

  @ApiPropertyOptional()
  readonly nextWeight?: string;

  @ApiPropertyOptional({ enum: statusProduct })
  readonly status?: string;

  @ApiPropertyOptional()
  readonly tradeMark?: string;

  @ApiPropertyOptional({ enum: productsTag })
  readonly tag?: string;

  @ApiPropertyOptional()
  readonly inCard?: boolean;

  @ApiPropertyOptional()
  readonly paymentCode?: string;

  @ApiPropertyOptional()
  readonly percentDiscount?: number;

  @ApiPropertyOptional()
  readonly quantitySold?: number;

  @ApiPropertyOptional()
  readonly calories?: string;

  @ApiPropertyOptional({ enum: UserStatus })
  readonly statusAccount?: string;

  @ApiPropertyOptional()
  readonly amount?: number;
}

export class ParamProductDto {
  @ApiPropertyOptional({ type: 'ObjectId'})
  readonly productID?: Types.ObjectId;
}

export class FindManyProductDto {
  @ApiPropertyOptional({ enum: SortBy })
  readonly sortBy?: SortBy = SortBy.id;

  @ApiPropertyOptional({ enum: sortDirection })
  readonly sortDirection?: SortDirection = SortDirection.asc;

  @ApiPropertyOptional()
  readonly limit?: number;

  @ApiPropertyOptional({ type: 'ObjectId'})
  readonly storeOwnerID?: Types.ObjectId;

  @ApiPropertyOptional()
  readonly offset?: number;

  @ApiPropertyOptional()
  readonly cursor?: string;

  @ApiPropertyOptional()
  readonly name?: string;

  @ApiPropertyOptional()
  readonly description?: string;

  @ApiPropertyOptional()
  readonly photos?: string;

  @ApiPropertyOptional()
  readonly price?: number;

  @ApiPropertyOptional()
  readonly origin?: string;

  @ApiPropertyOptional()
  readonly nextWeight?: string;

  @ApiPropertyOptional({ enum: statusProduct })
  readonly status?: string;

  @ApiPropertyOptional()
  readonly tradeMark?: string;

  @ApiPropertyOptional({ enum: productsTag })
  readonly tag?: string;

  @ApiPropertyOptional()
  readonly paymentCode?: string;

  @ApiPropertyOptional()
  readonly inCard?: boolean;

  @ApiPropertyOptional()
  readonly percentDiscount?: number;

  @ApiPropertyOptional()
  readonly quantitySold?: number;

  @ApiPropertyOptional()
  readonly calories?: string;

  @ApiPropertyOptional({ enum: UserStatus })
  readonly statusAccount?: string;

  @ApiPropertyOptional({ type: 'ObjectId'})
  readonly createdBy?: Types.ObjectId;

  @ApiPropertyOptional()
  readonly amount?: number;

  @ApiPropertyOptional()
  readonly filterAmount: number;

  @ApiPropertyOptional()
  readonly filterPrice: number;

  @ApiPropertyOptional()
  readonly filterQuantitySold?: number;

  @ApiPropertyOptional()
  readonly filterPercentDiscount?: number ;

}


export class ArrayProductsData {
  @ApiPropertyOptional({type:'productID[]'})
  readonly products?: string[];

  @ApiPropertyOptional({ type: DataUpdateProductDto })
  readonly data: DataUpdateProductDto

}