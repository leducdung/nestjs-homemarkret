import {
  Controller,
  Get,
  Request,
  Post,
  UseGuards,
  Body,
  Put,
  Param,
  Query,
  Delete,
} from '@nestjs/common';
import { JwtAuthGuard } from '../../auth/jwt/jwt-auth.guard';
import { AuthService } from '../../auth/auth.service';
import {
  CreateProductDto,
  FindManyProductDto,
  DataUpdateProductDto,
  ParamProductDto,
  ArrayProductsData
} from './model/products.dto';
import { ApiTags } from '@nestjs/swagger';
import { ProductsService } from './products.service';
import { ProductsCombinedService } from './products.combined.service';
import * as bcrypt from 'bcrypt';

@ApiTags('products')
@Controller('products')
export class ProductsController {
  constructor(
    private readonly productsService: ProductsService,
    private readonly productsCombinedService: ProductsCombinedService
    ) {}

  @Get('')
  async findMany(@Query() query: FindManyProductDto) {
    try {
      const product = await this.productsService.findMany({
        query,
      });

      return product;
    } catch (error) {
      throw error;
    }
  }

  @Get(':productID')
  async findOne(@Param() productID) {
    try {
      return this.productsService.findProduct({ _id: productID.productID });
    } catch (error) {
      throw error;
    }
  }

  @UseGuards(JwtAuthGuard)
  @Post('')
  async createOne(@Body() createProductDto: CreateProductDto, @Request() req) {
    try {
      return this.productsService.createOne({
        data: {
          ...createProductDto,
          createdBy: req.user.resultUser._id,
          storeOwnerID: req.user.resultUser.storeOwnerID._id,
        },
      });
    } catch (error) {
      throw error;
    }
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':productID')
  async Delete(@Param() productID, @Request() req) {
    try {
      return this.productsService.deleteOne({ _id: productID.productID });
    } catch (error) {
      throw error;
    }
  }

  @UseGuards(JwtAuthGuard)
  @Put(':productID')
  async updateOne(
    @Param() productID,
    @Body() data: DataUpdateProductDto,
    @Request() req,
  ) {
    try {
      return this.productsCombinedService.updateAndPushNotification({
        data,
        query: { _id: productID.productID },
        user: req.user
      });
    } catch (error) {
      throw error;
    }
  }

  @UseGuards(JwtAuthGuard)
  @Put('/likeAndUnLike/:productID')
  async likeOne(
    @Param() productID,
    @Request() req,
    ) {
    try {
      return this.productsCombinedService.likeAndUnLikeProduct({
        productID: productID.productID,
        userID:req.user.resultUser._id,
      });

    } catch (error) {
      throw error;
    }
  }

  @Get('getArayProduct/data')
  async getArrayProduct(
    @Query() query: FindManyProductDto
    ) {
    try {
     

      const array = await this.productsService.getArrayProduct({
        query,
      });

      return array;
    } catch (error) {
      throw error;
    }
  }

  @Put('updateMany/data')
  async updateMany(
    @Body() data: ArrayProductsData,
    ) {
    try {
      const arrayProduct = await this.productsService.updateMany({
        dataUpdate: data,
      });

      return arrayProduct;
    } catch (error) {
      throw error;
    }
  }
}
