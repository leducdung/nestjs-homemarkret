import { Document, Types } from 'mongoose';

export interface Notications extends Document {
  productID?: Types.ObjectId;
  commentAndRatingID?: Types.ObjectId;
  targetEntityName?:string
  targetID?: string
  body?: string;
  title?: string;
  description?: string;
  clickAction?: string;
  isRead?: boolean;
  event?: string;
  receiverUID?: Types.ObjectId
  receiverUStoreID?: Types.ObjectId
  createdBy?: Types.ObjectId;
  createdAt?: Date;
  updatedAt?: Date;
}

export const fieldNeedToUseRegex = ['title'];
