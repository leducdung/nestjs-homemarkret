import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
export type User = any;
import { InjectModel } from '@nestjs/mongoose';
import { getNextCursor } from '../../helpers/gets';
import { v1 as uuidv1 } from 'uuid';
import * as https from 'https'
import { sendHttpsRequest } from "./send_requestPayMent";
@Injectable()
export class Payment_momoService {
  constructor(
    // @InjectModel('Payments')
    // private readonly paymentsModel: Model<Payment>,
  ) {}

  async logData(reqData): Promise<any> {
    try {

      var endpoint = "https://test-payment.momo.vn/gw_payment/transactionProcessor"
      var hostname = "https://test-payment.momo.vn"
      var path = "/gw_payment/transactionProcessor"
      var partnerCode = "MOMOYTUU20210331"
      var accessKey = "K9rAYoI4cWBdK7fR"
      var serectkey = "gVwUvBnaIZJdXd9adUkEM2Cinp5PCjn8"
      var orderInfo = reqData.orderInfo
      var returnUrl = "http://production-hm-api.eu-4.evennode.com/payment/momo/checkPayment"
      var notifyurl = "https://callback.url/notify"
      var amount = reqData.amount.toString()
      var orderId = uuidv1()
      var requestId = uuidv1()
      var requestType = "captureMoMoWallet"
      var extraData = "merchantName;merchantId" //pass empty value if your merchant does not have stores else merchantName=[storeName]; merchantId=[storeId] to identify a transaction map with a physical store

      //before sign HMAC SHA256 with format
      //partnerCode=$partnerCode&accessKey=$accessKey&requestId=$requestId&amount=$amount&orderId=$oderId&orderInfo=$orderInfo&returnUrl=$returnUrl&notifyUrl=$notifyUrl&extraData=$extraData
      var rawSignature = "partnerCode=" + partnerCode + "&accessKey=" + accessKey + "&requestId=" + requestId + "&amount=" + amount + "&orderId=" + orderId + "&orderInfo=" + orderInfo + "&returnUrl=" + returnUrl + "&notifyUrl=" + notifyurl + "&extraData=" + extraData
      //puts raw signature
      // console.log("--------------------RAW SIGNATURE----------------")
      // console.log('rawSignature' + rawSignature)
      //signature
      var crypto = require('crypto');
      var signature = crypto.createHmac('sha256', serectkey)
        .update(rawSignature)
        .digest('hex');
      // console.log("--------------------SIGNATURE----------------")

      // console.log(signature)

      //json object send to MoMo endpoint
      var body = JSON.stringify({
        partnerCode: partnerCode,
        accessKey: accessKey,
        requestId: requestId,
        amount: amount,
        orderId: orderId,
        orderInfo: orderInfo,
        returnUrl: returnUrl,
        notifyUrl: notifyurl,
        extraData: extraData,
        requestType: requestType,
        signature: signature,
      })
      //Create the HTTPS objects
      var options = {
        hostname: 'test-payment.momo.vn',
        port: 443,
        path: '/gw_payment/transactionProcessor',
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Content-Length': Buffer.byteLength(body)
        }
      };


      //Send the request and get the response
      const data = await sendHttpsRequest(options, body)

      return data;
    } catch (error) {
      return Promise.reject(error);
    }
  }


  async logDataResult(): Promise<any> {
    try {
      var endpoint = "https://test-payment.momo.vn/gw_payment/transactionProcessor"
      var hostname = "https://test-payment.momo.vn"
      var path = "/gw_payment/transactionProcessor"
      var partnerCode = "MOMOYTUU20210331"
      var accessKey = "K9rAYoI4cWBdK7fR"
      var serectkey = "gVwUvBnaIZJdXd9adUkEM2Cinp5PCjn8"
      var orderInfo = "pay with MoMo"
      var returnUrl = "https://localhost:3000/payment"
      var notifyurl = "https://callback.url/notify"
      var amount = "10000"
      var orderId = uuidv1()
      var requestId = uuidv1()
      var requestType = "captureMoMoWallet"
      var extraData = "merchantName;merchantId" //pass empty value if your merchant does not have stores else merchantName=[storeName]; merchantId=[storeId] to identify a transaction map with a physical store

      //before sign HMAC SHA256 with format
      //partnerCode=$partnerCode&accessKey=$accessKey&requestId=$requestId&amount=$amount&orderId=$oderId&orderInfo=$orderInfo&returnUrl=$returnUrl&notifyUrl=$notifyUrl&extraData=$extraData
      var rawSignature = "partnerCode=" + partnerCode + "&accessKey=" + accessKey + "&requestId=" + requestId + "&amount=" + amount + "&orderId=" + orderId + "&orderInfo=" + orderInfo + "&returnUrl=" + returnUrl + "&notifyUrl=" + notifyurl + "&extraData=" + extraData
      //puts raw signature
      console.log("--------------------RAW SIGNATURE----------------")
      console.log('rawSignature' + rawSignature)
      //signature
      var crypto = require('crypto');
      var signature = crypto.createHmac('sha256', serectkey)
        .update(rawSignature)
        .digest('hex');
      console.log("--------------------SIGNATURE----------------")

      console.log(signature)

      //json object send to MoMo endpoint
      var body = JSON.stringify({
        partnerCode: partnerCode,
        accessKey: accessKey,
        requestId: requestId,
        amount: amount,
        orderId: orderId,
        orderInfo: orderInfo,
        returnUrl: returnUrl,
        notifyUrl: notifyurl,
        extraData: extraData,
        requestType: requestType,
        signature: signature,
      })
      //Create the HTTPS objects
      var options = {
        hostname: 'test-payment.momo.vn',
        port: 443,
        path: '/gw_payment/transactionProcessor',
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Content-Length': Buffer.byteLength(body)
        }
      };


      //Send the request and get the response
      const data = await sendHttpsRequest(options, body)

      return data;
    } catch (error) {
      return Promise.reject(error);
    }
  }

}
