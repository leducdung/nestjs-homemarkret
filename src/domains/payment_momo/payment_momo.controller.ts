import {
  Controller,
  Get,Post
} from '@nestjs/common';
import { Payment_momoService } from './payment_momo.service';

@Controller('')
export class Payment_momoController {
  constructor(private readonly payment_momoService: Payment_momoService) {}

  @Post('Payment_momo')
  async findMany(
  ) {
    try {
      const reqData =1
      const data = await this.payment_momoService.logData(reqData);

      return data;
    } catch (error) {
      throw error;
    }
  }

  @Post('Payment_momoResult')
  async Payment_momoResult(
  ) {
    try {
      const data = await this.payment_momoService.logDataResult( );

      return data;
    } catch (error) {
      throw error;
    }
  }
}
