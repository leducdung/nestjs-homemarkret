import * as https from 'https'
import { stringify as queryStringify } from 'querystring'
export interface RequestOptions {
  url?: string
  localAddress?: string
  socketPath?: string
  method?: string
  headers?: Headers
  auth?: string
  params?: ParsedUrlQueryInput
  timeout?: number
  setHost?: boolean
  rejectUnauthorized?: boolean
  servername?: string
  body?: object
}
export interface Headers {
  [header: string]: number | string | string[] | undefined
}
export interface LiteralObject {
  [key: string]: string | string[] | number | object | undefined
}
export interface Response {
  status: number
  body: LiteralObject
}

interface ParsedUrlQueryInput extends NodeJS.Dict<string | number | boolean | ReadonlyArray<string> |
ReadonlyArray<number> | ReadonlyArray<boolean> | null> {
}

export const MethodsNeedToWriteData = ['POST', 'PUT']

export const sendHttpsRequest = async (options , body): Promise<Response> => {
  return new Promise((resolve, reject) => {
    const data = body
    const req = https.request(options, (res) => {
      const statusCode = res.statusCode || 500
      let responseBody = ''
      res.on('data', (chunk) => {
        responseBody += chunk
      })
      res.on('end', () => {
        { resolve({
          status: statusCode,
          body: JSON.parse(responseBody),
        })}
      })
    })
    req.on('error', (err) => {
      reject(err)
    })
    if (data) {
      req.write(data)
    }
    req.end()
  })
}
