import { Injectable } from '@nestjs/common';
import { EmployeesService } from '../employees/employees.service';
import { StoreOwnersService } from '../storeOwners/storeOwners.service';
import { UsersService } from './users.service';
import { Users } from './model/users.interface';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UsersCombinedService {
  constructor(
    private readonly employeesService: EmployeesService,
    private readonly storeOwnersService: StoreOwnersService,
    private readonly usersService: UsersService,
  ) {}

  async createOneUserAndAcess({ data }): Promise<Users | undefined> {
    try {
      const saltOrRounds = await 10;

      const hash = await bcrypt.hash(data.passWord, saltOrRounds);



      const user = await this.usersService.createOne({
        data: {
          ...data,
          userName: data.userName,
          passWord: hash,
        },
      });

     if(user){
      const storeOwner = await this.storeOwnersService.createOne({
        data,
      });

      await this.usersService.updateOne({
        data:{
          storeOwnerID: storeOwner._id,
        },
        query :{ _id: user._id },
      });
     }

      return await user;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async UpdateOneUserAndAcess({ data, query }): Promise<Users | any> {
    try {
      const user = await this.usersService.findOneUser(query);

      if (data.passWord) {
        const saltOrRounds = await 10;

        const hash = await bcrypt.hash(data.passWord, saltOrRounds);

        await this.usersService.updateOne({
          data: {
            passWord: hash,
          },
          query,
        });
      }

      if (!data.passWord) {
        await this.usersService.updateOne({
          data,
          query,
        });
      }

      if (data.rolePending === 'STOREOWNER' && data.rolePending != user.role) {
        await this.storeOwnersService.updateOne({
          data: { status: 'PENDING' },
          query: { _id: user.storeOwnerID },
        });
      }

      if (data.role === 'STOREOWNER') {
        await this.storeOwnersService.updateOne({
          data: { status: 'NOSTATUS' },
          query: { _id: user.storeOwnerID },
        });
      }

      const userUpdated = await this.usersService.findOneUser(query);

      return await userUpdated;
    } catch (error) {
      return Promise.reject(error);
    }
  }


  async ComparePassWord({ data, query }){
    try {

      const user = await this.usersService.findOneUser({
         _id: query._id
        })


        const isMatch = await bcrypt.compare(data.passWord, user.passWord);

        if(!isMatch){
           return { message: 'Mật khẩu không đúng' };
        }

        if (data.newPassWord !== data.comparePassWord) {
            return {message: 'Mật khẩu không không trùng khớp' }
        }

      const userUpdated = await this.UpdateOneUserAndAcess({
        data : {
          passWord: data.newPassWord
        },
        query: { _id: query._id },
      })

      return userUpdated
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async  register_storeOwner({ data, query }) : Promise<any> {
    try {


      if( !data.email  || !data.name || !data.description || !data.phoneNumbers || !data.address ){
        return {
          mess: 'không được để trống mục nào'
        }
      }

       await this.usersService.updateOne({
        data: {
          rolePending:"STOREOWNER",
          fullName: data.name,
          profilePicture: data.photos,
          address:data.address,
          phone:data.phoneNumbers,
        },
        query: { _id: query._id },
      })

      await this.storeOwnersService.updateOne({
        data: {
          status:"PENDING",
          description:data.description,
          photos:data.photos,
          address:data.address,
          phoneNumbers:data.phoneNumbers,
          name:data.name,
          email:data.email
        },
        query: { _id: query.storeOwnerID },
      })

      return {
        mess: 'ok'
      }
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async  config_storeOwner({data}) {
    try {

      console.log(data.userID);

      const user =  await this.usersService.findOneUser({ _id: data.userID })
      console.log(user);

       await this.usersService.updateOne({
        data: {
          role:"STOREOWNER",
        },
        query: { _id: user._id },
      })

      await this.storeOwnersService.updateOne({
        data: {
          status:"ACCEPTED",
        },
        query: { _id: user.storeOwnerID },
      })

      return  {
        mess: 'ok'
      };

    } catch (error) {
      return Promise.reject(error);
    }
  }
}
