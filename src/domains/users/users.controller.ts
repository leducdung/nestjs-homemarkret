import {
  Controller,
  Get,
  Request,
  Post,
  UseGuards,
  Body,
  Put,
  Param,
  Query,
} from '@nestjs/common';
import { JwtAuthGuard } from '../../auth/jwt/jwt-auth.guard';
import { LocalAuthGuard } from '../../auth/local/local-auth.guard';
import { AuthService } from '../../auth/auth.service';
import {
  CreateUserDto,
  FindManyDocumentsDto,
  ParamUserDto,
  DataUpdateUserDto,
  DataUpdatePassWordDto,
  SubscribeTokenDto,
  CreateStoreDto,
} from './model/users.dto';
import { ApiTags } from '@nestjs/swagger';
import { UsersService } from './users.service';
import { UsersCombinedService } from './users.combined.service';
import * as bcrypt from 'bcrypt';
import { FireBaseService } from '../fireBase/firebase.service'

@ApiTags('auth')
@Controller()
export class UsersController {
  constructor(
    private readonly usersService: UsersService,
    private readonly authService: AuthService,
    private readonly usersCombinedService: UsersCombinedService,
    private readonly fireBaseService: FireBaseService,
  ) {}

  @UseGuards(JwtAuthGuard)
  @Get('profile/findMany')
  async findMany(@Query() query: FindManyDocumentsDto, @Request() { req }) {
    try {
      const users = await this.usersService.findMany({
        query,
      });

      return users;
    } catch (error) {
      throw error;
    }
  }

  @UseGuards()
  @Post('register')
  async createOne(@Body() CreateUserDto: CreateUserDto, @Request() { user }) {
    try {
      return await this.usersCombinedService.createOneUserAndAcess({
        data: CreateUserDto,
      });
    } catch (error) {
      return error;
    }
  }

  @UseGuards()
  @Post('/login')
  async login(@Body() createUserDto: CreateUserDto, @Request() { user }) {
    try {
      return this.authService.login(createUserDto);
    } catch (error) {
      throw error;
    }
  }

  @UseGuards(JwtAuthGuard)
  @Get('profile')
  getProfile(@Request() req) {
    return req.user;
  }

  @UseGuards(JwtAuthGuard)
  @Get('profile/:userID')
  async findOne(@Param() userID: ParamUserDto, @Request() req) {
    try {
      return this.usersService.findOneUser({ _id: userID.userID });
    } catch (error) {
      throw error;
    }
  }

  @UseGuards(JwtAuthGuard)
  @Put('profile/:userID')
  async updateOne(
    @Param() userID: ParamUserDto,
    @Body() data: DataUpdateUserDto,
    @Request() req,
  ) {
    try {
      return this.usersCombinedService.UpdateOneUserAndAcess({
        data,
        query: { _id: userID.userID },
      });
    } catch (error) {
      throw error;
    }
  }

  @UseGuards(JwtAuthGuard)
  @Put('comparePassWord')
  async comparePassWord(
    @Body() data: DataUpdatePassWordDto,
    @Request() req,
  ) {
    try {

      return this.usersCombinedService.ComparePassWord({
        data,
        query: { _id: req.user.resultUser._id },
      });
    } catch (error) {
      throw error;
    }
  }

  @UseGuards(JwtAuthGuard)
  @Post('fcm/subscribe')
  async subscribeToken(
    @Body() data : SubscribeTokenDto,
    @Request() { user },
  ) {
    try {

      const isSubscribeTokenToTopic = await this.fireBaseService.subscribeTokenToTopic({
        tokens: [data.token],
        topic : `User-${user.resultUser._id}`,
      })


      return isSubscribeTokenToTopic

    } catch (error) {
      throw error
    }
  }

  @UseGuards(JwtAuthGuard)
  @Post('fcm/unsubscribe')
  async unSubscribeToken(
    @Body() data : SubscribeTokenDto,
    @Request() { user },
  ) {
    try {

      const isSubscribeTokenToTopic = await this.fireBaseService.unsubscribeTokenFromTopic({
        tokens: [data.token],
        topic : `User-${user.resultUser._id}`,
      })

      return isSubscribeTokenToTopic

    } catch (error) {
      throw error
    }
  }

  @UseGuards(JwtAuthGuard)
  @Post('register_storeOwner')
  async register_storeOwner(@Body() register_storeOwner: CreateStoreDto, @Request() { user }) {
    try {
      const data =  await this.usersCombinedService.register_storeOwner({
        data:register_storeOwner,
        query: {
          _id: user.resultUser._id,
          storeOwnerID:   user.resultUser.storeOwnerID._id,
         },
      });

      return data
    } catch (error) {
      return error;
    }
  }


  @UseGuards(JwtAuthGuard)
  @Post('config_storeOwner')
  async config_storeOwner(@Body() userID: ParamUserDto, @Request() { user }) {
    try {
      return await this.usersCombinedService.config_storeOwner({data:userID});
    } catch (error) {
      return error;
    }
  }

}
