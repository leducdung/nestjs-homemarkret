interface MessageData {
  messageID: string
  click_action: string
  content: string
}

interface NotificationData {
  body: string
  title: string
  click_action: string
}

export interface SendMessageToDevices {
  tokens: string[]
  notification?: NotificationData
  message?: MessageData
  data: FirebaseData
}

export interface SendMessageToTopic {
  topic: string
  notification?: NotificationData
  message?: MessageData
  data?: FirebaseData
}

export interface SendMessage {
  notification?: NotificationData
  message?: MessageData
  data?: FirebaseData
}
interface FirebaseData {
  createdBy?: string
  notificationID?: string
  isRead?: string
  event?: string
  userPhoto?: string
  username?: string
  createdAt?: string
  data?: string
  deniedReason?: string
  storeOffers?: string
  employeeID?: string
  customerID?: string
  editedAmount?: string
  companyID?: string
  departmentID?: string
  taskID?: string
  title?: string
  body?: string
  clickAction?: string
  receiverUID?: string
  updatedAt?: string
}

export interface SubscribeTokenToTopic {
  tokens: string[]
  topic: string
}

export interface UnsubscribeTokenToTopic {
  tokens: string[]
  topic: string
}