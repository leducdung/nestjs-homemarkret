import { Injectable } from '@nestjs/common'
import { credential, initializeApp, messaging } from 'firebase-admin'
import {
  SendMessageToDevices, SendMessageToTopic, SubscribeTokenToTopic,
  UnsubscribeTokenToTopic,
} from './mod‎els‎/firebase.interface'
import { FirebaseConstants } from '../../constains/firebase'
import { key } from './key';

@Injectable()
export class FireBaseService {
  constructor() {
    initializeApp({
      credential: credential.cert({
        projectId: key.project_id,
        clientEmail: key.client_email,
        privateKey: key.private_key,
      }),
      databaseURL: "https://capstone2-24fd3-default-rtdb.firebaseio.com",
    })
  }


  async sendMessageToDevices({ tokens, notification, message, data = {} }: SendMessageToDevices): Promise<boolean> {
    try {

      if (!notification && !message) {
        return Promise.reject('Notification or message is missing')
      }

      const payload: messaging.MessagingPayload = {}

      if (notification) {
        payload.notification = { ...notification }
        payload.data = { ...data }
      }

      if (message) {
        payload.data = {
          ...data,
          ...message,
        }
      }

      const options = {
        priority: 'normal',
        timeToLive: FirebaseConstants.timeToLive,
      }

      await messaging().sendToDevice(tokens, payload, options)

      return true
    } catch (error) {
      return Promise.reject(error)
    }
  }

  async sendMessageToTopic({ topic, notification, message, data = {} }: SendMessageToTopic): Promise<boolean> {
    try {



      if (!notification && !message) {
        return Promise.reject('Notification or message is missing')
      }

      const payload: messaging.MessagingPayload = {}

      if (notification) {
        payload.notification = { ...notification }
        payload.data = { ...data }
      }

      if (message) {
        payload.data = {
          ...data,
          ...message,
        }
      }

      const options = {
        priority: 'normal',
        timeToLive: FirebaseConstants.timeToLive,
      }

      await messaging().sendToTopic(topic, payload, options)
  

      return true
    } catch (error) {
      return Promise.reject(error)
    }
  }

  async subscribeTokenToTopic({ tokens, topic }: SubscribeTokenToTopic): Promise<boolean> {
    try {

      if (!Array.isArray(tokens) || tokens.length === 0) {
        return
      }

      await messaging().subscribeToTopic(tokens, topic)

      return true
    } catch (error) {
      return Promise.reject(error)
    }
  }

  async unsubscribeTokenFromTopic({ tokens, topic }: UnsubscribeTokenToTopic): Promise<boolean> {
    try {

      if (!Array.isArray(tokens) || tokens.length === 0) {
        return
      }

      await messaging().unsubscribeFromTopic(tokens, topic)

      return true
    } catch (error) {
      return Promise.reject(error)
    }
  }

}