import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
export type User = any;
import {
  fieldNeedToUseRegex,
  Payment,
} from './model/payment.interface';
import { InjectModel } from '@nestjs/mongoose';
import { getNextCursor } from '../../helpers/gets';
import { buildFindingQuery , generateChartData} from '../../helpers/build';
import { Types } from 'mongoose';

@Injectable()
export class PaymentService {
  constructor(
    @InjectModel('Payments')
    private readonly paymentsModel: Model<Payment>,
  ) { }

  makeid(length) {
    let result = '';
    const characters =
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  async findOne({ data }: any): Promise<Payment | string> {
    try {
      const payment = await this.paymentsModel
        .findOne(data)
        .populate('createdBy')
        .populate('userCreatedProductID')
        .populate('store_Owners')
        .populate('employeeID')
        .exec();

      if (!payment) {
        return 'payment not found';
      }

      return payment;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async findProduct(query): Promise<Payment | string> {
    try {
      const payment = await this.paymentsModel
        .findOne(query)
        .populate('productID')
        .populate('createdBy')
        .populate('userCreatedProductID')
        .populate('employeeID')
        .exec();

      if (!payment) {
        return 'Payment not found';
      }

      return payment;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async createOne({ data }: any): Promise<Payment | undefined> {
    try {
      const payment = await new this.paymentsModel({
        ...data,
        paymentCode: this.makeid(5),
      });

      await payment.save();

      return await payment;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async updateOne({ data, query }): Promise<Payment | string> {
    try {
      const payment = await this.paymentsModel
        .findOne(query)
        .populate('productID')
        .populate('createdBy')
        .populate('store_Owners')
        .exec();

      if (!payment) {
        return 'payment Not Found';
      }

      Object.assign(payment, data);

      await payment.save();

      return payment;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async deleteOne(query): Promise<boolean | string> {
    try {
      const payment = await this.paymentsModel.findOne(query).exec();

      if (!payment) {
        return 'payment not found';
      }

      await this.paymentsModel.deleteOne(query).exec();

      return true;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async findMany({ query }) {
    try {
      const {
        limit,
        sortBy = '_id',
        offset = 0,
        ...queryWithoutSortByAndLimit
      } = query;

      const promises = [];

      const {
        sortingCondition,
        findingQuery,
        findAllQuery,
        hasPage,
      } = buildFindingQuery({
        query: {
          ...queryWithoutSortByAndLimit,
          sortBy,
          limit,
        },
        fieldNeedToUseRegex,
      });

      const limits = parseInt(limit);
      const skip = parseInt(offset);
      if (hasPage) {
        promises.push(
          this.paymentsModel
            .find(findingQuery)
            .populate('productID')
            .populate('createdBy')
            .populate('store_Owners')
            .sort(sortingCondition)
            .limit(limits)
            .skip(skip)
            .exec(),
          this.paymentsModel.countDocuments(findAllQuery).exec(),
        );
      }

      if (!hasPage) {
        promises.push(
          this.paymentsModel
            .find(findingQuery)
            .populate('productID')
            .populate('createdBy')
            .populate('store_Owners')
            .exec(),
          this.paymentsModel.countDocuments(findAllQuery).exec(),
        );
      }

      const [ payments, totalCount ] = await Promise.all(promises);

      if (!payments || !payments.length) {
        return {
          cursor: 'END',
          totalCount,
          list: [],
        };
      }

      const nextCursor = getNextCursor({
        data: payments,
        sortBy,
      });

      return {
        cursor: nextCursor,
        totalCount,
        list: payments,
      };
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async getArrayProduct(query) {
    try {
      const payments = await this.findMany(query)

      const dataArray = []

      payments.list.forEach(element => {
        dataArray.push(element._id)
      });
      return dataArray;

    } catch (error) {
      return Promise.reject(error);
    }
  }



  async updateMany({ dataUpdate }) {
    try {
      dataUpdate.payments.forEach(async (element) => {
        const payment: any = await this.findProduct({ _id: element })

        await this.updateOne({
          data: {
            ...dataUpdate.data,
            productIDs: payment.productIDs,
          },
          query: { _id: element },
        })
         

      });

      return true

    } catch (error) {
      return Promise.reject(error);
    }
  }

  async findManyAggregate({ query }) {
    try {
      const {
        limit,
        sortBy = '_id',
        offset = 0,
        ...queryWithoutSortByAndLimit
      } = query;

      const promises = [];

      const {
        sortingCondition,
        findingQuery,
        findAllQuery,
        hasPage,
      } = buildFindingQuery({
        query: {
          ...queryWithoutSortByAndLimit,
          sortBy,
          limit,
        },
        convertToObjectIdFields: [
          'userCreatedProductID',
          'employeeID',
          '_id',
          'createdBy',
        ],
      });

      const limits = parseInt(limit);
      const skip = parseInt(offset);
      if (hasPage) {
        promises.push(
          this.paymentsModel.aggregate([
            { $match: findingQuery },
            { $sort: sortingCondition },
            {
              $group: {
                _id: {
                  date: { $dateToString: { format: "%Y-%m-%d", date: "$createdAt" } },
                  sender: '$sender'
                },
                count: { $sum: '$totalMoney' }
              }
            },
            {
              $group: {
                _id: '$_id.sender',
                arr: { $push: { date: '$_id.date', count: '$count' } }
              }
            }
          ]),
          this.paymentsModel.aggregate([
            { $match: findAllQuery },
            {
              $count: 'count',
            },
          ]),
        )
      }

      if (!hasPage) {
        promises.push(
          this.paymentsModel.aggregate([
            { $match: findingQuery },
            { $sort: sortingCondition },
            {
              $group: {
                _id: { $dateToString: { format: "%Y-%m-%d", date: "$createdAt" } },
                count: { $sum: '$totalMoney' }
              }
            }
          ]),
          this.paymentsModel.aggregate([
            { $match: findAllQuery },
            {
              $count: 'count',
            },
          ]),
        )
      }

      const [ payments, countInFinding ] = await Promise.all(promises);


      const totalCount = countInFinding && countInFinding[0] && countInFinding[0].count


      if (!payments || !payments.length) {
        return {
          cursor: 'END',
          totalCount,
          list: [],
        };
      }

      return {
        totalCount,
        list: generateChartData(payments)
      };
    } catch (error) {
      return Promise.reject(error);
    }
  }

}
