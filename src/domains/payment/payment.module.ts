import { Module, forwardRef } from '@nestjs/common';
import { PaymentService } from './payment.service';
import { MongooseModule } from '@nestjs/mongoose';
import { PaymentSchema } from './model/payment.schema';
import { AuthModule } from '../../auth/auth.module';
import { databaseModule } from '../../database/database.module';
import { PaymentController } from './payment.controller';
import { PaymentCombinedService } from './payment.combined.service';
import { Payment_momoModule } from "../payment_momo/payment_momo.module";
import { ProductsModule } from "../products/products.module";
import { StoreOwnersModule } from "../storeOwners/storeOwners.module";
import { UsersModule } from "../users/users.module";
import { FirebaseModule } from "../fireBase/firebase.module";
import { NoticationsModule } from "../notications/notications.module";

export const historyOderUserModel = MongooseModule.forFeature([
  {
    name: 'Payments',
    schema: PaymentSchema,
  },
]);

@Module({
  imports: [
    forwardRef(() => AuthModule),
    forwardRef(() => databaseModule),
    forwardRef(() => historyOderUserModel),
    forwardRef(() => Payment_momoModule),
    forwardRef(() => ProductsModule),
    forwardRef(() => StoreOwnersModule),
    forwardRef(() => UsersModule),
    forwardRef(() => FirebaseModule),
    forwardRef(() => NoticationsModule),
  ],
  controllers: [PaymentController],
  providers: [PaymentService, PaymentCombinedService],
  exports: [PaymentService, PaymentCombinedService],
})
export class PaymentModule {}
