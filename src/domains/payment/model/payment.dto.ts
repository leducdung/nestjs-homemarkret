import { ApiPropertyOptional } from '@nestjs/swagger';
import { Types } from 'mongoose';
import { sortDirection, SortDirection } from '../../../constains/common';
import {
  PaymentStatus,
  GiaoHangStatus,
  OrderStatus,
} from '../../../constains/common';

export enum SortBy {
  id = '_id',
  title = 'title',
  createdAt = 'createdAt',
  updatedAt = 'updatedAt',
}

export class ProductIDsData {
  @ApiPropertyOptional({type:'string'})
  readonly productID?: Types.ObjectId;

  @ApiPropertyOptional()
  readonly amount?: Number

  @ApiPropertyOptional()
  readonly name?: string

  @ApiPropertyOptional()
  readonly photo?: string
}

export class CreatePaymentDto {
  @ApiPropertyOptional({type :[ProductIDsData]})
  readonly productIDs?: Types.ObjectId[];

  @ApiPropertyOptional()
  readonly userCreatedProductID?: Types.ObjectId;

  @ApiPropertyOptional()
  readonly employeeID?: Types.ObjectId;

  @ApiPropertyOptional()
  readonly store_Owners?: Types.ObjectId;

  @ApiPropertyOptional()
  readonly requestId?: Types.ObjectId;

  @ApiPropertyOptional( )
  readonly deliveryAddress?: string;

  @ApiPropertyOptional( )
  readonly deliveryPhoneNumber?: string;

  @ApiPropertyOptional()
  readonly totalMoney?: Number

  @ApiPropertyOptional( )
  readonly transactionID?: string;

  @ApiPropertyOptional( )
  readonly amount?: number;

  @ApiPropertyOptional( )
  readonly title?: string;

  @ApiPropertyOptional({enum:PaymentStatus})
  readonly paymentStatus?: PaymentStatus.PENDING;

  @ApiPropertyOptional({enum:GiaoHangStatus})
  readonly giaoHangStatus?: GiaoHangStatus.GIAO_HANG;

  @ApiPropertyOptional( )
  readonly tip?: number;

  @ApiPropertyOptional( )
  readonly fee?: number;

  @ApiPropertyOptional( )
  readonly discountAmount?: number;

  @ApiPropertyOptional( )
  readonly event?: string;

  @ApiPropertyOptional({enum:OrderStatus})
  readonly order?: OrderStatus.PREPARING_ORDER;

  @ApiPropertyOptional()
  readonly errorCode?: string;

}

export class DataUpdatePaymentDto {
  @ApiPropertyOptional({type :[ProductIDsData]})
  readonly productIDs?: Types.ObjectId[];

  @ApiPropertyOptional()
  readonly createdBy?: Types.ObjectId;

  @ApiPropertyOptional()
  readonly totalMoney?: Number

  @ApiPropertyOptional()
  readonly userCreatedProductID?: Types.ObjectId;

  @ApiPropertyOptional()
  readonly store_Owners?: Types.ObjectId;

  @ApiPropertyOptional()
  readonly employeeID?: Types.ObjectId;

  @ApiPropertyOptional()
  readonly requestId?: Types.ObjectId;

  @ApiPropertyOptional( )
  readonly orderId?: string;

  @ApiPropertyOptional( )
  readonly payUrl?: string;

  @ApiPropertyOptional( )
  readonly signature?: string;

  @ApiPropertyOptional( )
  readonly transactionID?: string;

  @ApiPropertyOptional( )
  readonly deliveryAddress?: string;

  @ApiPropertyOptional( )
  readonly deliveryPhoneNumber?: string;

  @ApiPropertyOptional( )
  readonly amount?: number;

  @ApiPropertyOptional( )
  readonly title?: string;

  @ApiPropertyOptional({enum:PaymentStatus})
  readonly paymentStatus?: PaymentStatus.PENDING;

  @ApiPropertyOptional({enum:GiaoHangStatus})
  readonly giaoHangStatus?: GiaoHangStatus.GIAO_HANG;

  @ApiPropertyOptional( )
  readonly tip?: number;

  @ApiPropertyOptional( )
  readonly fee?: number;

  @ApiPropertyOptional( )
  readonly discountAmount?: number;

  @ApiPropertyOptional( )
  readonly event?: string;

  @ApiPropertyOptional()
  readonly errorCode?: string;

  @ApiPropertyOptional({enum:OrderStatus})
  readonly order?: OrderStatus.PREPARING_ORDER;
}

export class ParamPaymentDto {
  @ApiPropertyOptional()
  readonly paymentID?: Types.ObjectId;

}

export class FindPaymentDto {
  @ApiPropertyOptional({ enum: SortBy })
  readonly sortBy?: SortBy = SortBy.id;

  @ApiPropertyOptional({ enum: sortDirection })
  readonly sortDirection?: SortDirection = SortDirection.asc;

  @ApiPropertyOptional()
  readonly limit?: number;

  @ApiPropertyOptional()
  readonly offset?: number;

  @ApiPropertyOptional()
  readonly cursor?: string;

  @ApiPropertyOptional({type :[ProductIDsData]})
  readonly productIDs?: Types.ObjectId[];

  @ApiPropertyOptional({type:'id của người mua sản phẩm'})
  readonly createdBy?: Types.ObjectId;

  @ApiPropertyOptional({type:'id của chủ cửa hàng, bảng user'})
  readonly userCreatedProductID?: Types.ObjectId;

  @ApiPropertyOptional({type:'id của nhân viên, dùng để tab nhân viên đó soạn món hàng này'})
  readonly employeeID?: Types.ObjectId;

  @ApiPropertyOptional()
  readonly store_Owners?: Types.ObjectId;

  @ApiPropertyOptional({type:'dùng để nhận biết đơn hàng này là của ai, hãy cho cửa hàng thấy mã code này khi đến nhận hàng'})
  readonly paymentCode?: string;

  @ApiPropertyOptional( )
  readonly deliveryAddress?: string;

  @ApiPropertyOptional( )
  readonly deliveryPhoneNumber?: string;

  @ApiPropertyOptional()
  readonly requestId?: Types.ObjectId;

  @ApiPropertyOptional( )
  readonly orderId?: string;

  @ApiPropertyOptional( )
  readonly payUrl?: string;

  @ApiPropertyOptional( )
  readonly signature?: string;

  @ApiPropertyOptional( )
  readonly transactionID?: string;

  @ApiPropertyOptional( )
  readonly amount?: number;

  @ApiPropertyOptional( )
  readonly title?: string;

  @ApiPropertyOptional({enum:PaymentStatus})
  readonly paymentStatus?: PaymentStatus.PENDING;

  @ApiPropertyOptional({enum:GiaoHangStatus})
  readonly giaoHangStatus?: GiaoHangStatus.GIAO_HANG;

  @ApiPropertyOptional( )
  readonly tip?: number;

  @ApiPropertyOptional( )
  readonly fee?: number;

  @ApiPropertyOptional()
  readonly totalMoney?: Number

  @ApiPropertyOptional( )
  readonly discountAmount?: number;

  @ApiPropertyOptional( )
  readonly event?: string;

  @ApiPropertyOptional()
  readonly errorCode?: string;

  @ApiPropertyOptional()
  readonly createdAt?: string;

  @ApiPropertyOptional()
  readonly updatedAt?: string;

  @ApiPropertyOptional({enum:OrderStatus})
  readonly order?: OrderStatus.PREPARING_ORDER;
}

export class UpdateManyPaymentsData {
  @ApiPropertyOptional({type:'productID[]'})
  readonly payments?: string[];

  @ApiPropertyOptional({ type: DataUpdatePaymentDto })
  readonly data: DataUpdatePaymentDto

}

export class UpdateOrderData {

  @ApiPropertyOptional({enum:OrderStatus})
  readonly order?: OrderStatus.PREPARING_ORDER;

}