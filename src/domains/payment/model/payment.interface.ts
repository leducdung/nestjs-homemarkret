import { Document, Types } from 'mongoose';
import { GGCombinedService } from 'src/domains/o-auth-google/o-auth-google.combined.service';
import { PaymentStatus, GiaoHangStatus, OrderStatus} from '../../../constains/common';

export interface ProductIDs extends Document {
  productID?:String
  amount?: Number
  name?: String
  photo?: String
}

export interface Payment extends Document {
  productIDs?: ProductIDs[];
  createdBy?: Types.ObjectId;
  userCreatedProductID?: Types.ObjectId;
  store_Owners?: Types.ObjectId;
  employeeID?: Types.ObjectId;
  transactionID?: String
  requestId?: String
  orderId?: String
  payUrl?: String
  signature?: String
  deliveryAddress?: String
  deliveryPhoneNumber?: String
  title?: String
  description?: String
  paymentStatus?: PaymentStatus
  giaoHangStatus?: GiaoHangStatus
  tip ?: Number
  fee ?: Number
  paymentCode?:string
  totalMoney?: Number
  discountAmount ?: Number
  event?: String
  error ?: String
  order?: OrderStatus
  createdAt?: Date;
  updatedAt?: Date;
}

export const fieldNeedToUseRegex = ['title'];
