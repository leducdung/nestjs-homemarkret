
import * as mongoose from 'mongoose';
import {
   PaymentStatus,
   GiaoHangStatus,
   OrderStatus,
} from '../../../constains/common';

const {
  Types: { ObjectId },
} = mongoose.Schema;

export const PaymentSchema = new mongoose.Schema(
  {
    productIDs: [{
      productID:{ type: String, ref: 'Products', default: [] },
      amount: { type: Number, default: null },
      name: { type: String, default: null },
      photo: { type: String, default: null },
    }],
    createdBy: { type: ObjectId, ref: 'Users' },
    userCreatedProductID: { type: ObjectId, ref: 'Users', default: null, required: true },
    store_Owners: { type: ObjectId, ref: 'Store_Owners', default: null},
    employeeID: { type: ObjectId, ref: 'Employees', default: null },
    requestId: { type: String, default: null },
    orderId: { type: String, default: null },
    payUrl: { type: String, default: null },
    deliveryAddress: { type: String, default: null },
    deliveryPhoneNumber: { type: String, default: null },
    signature: { type: String, default: null },
    transactionID: { type: String, default: null },
    description: { type: String, default: null },
    title: { type: String, default: null },
    paymentCode: { type: String, default: null },
    paymentStatus: { type: String, enum: PaymentStatus,  default: PaymentStatus.PENDING },
    giaoHangStatus:{ type: String, enum: GiaoHangStatus,  default: GiaoHangStatus.GIAO_HANG },
    totalMoney : { type: Number, default: null },
    tip : { type: Number, default: null },
    fee : { type: Number, default: null },
    discountAmount : { type: Number, default: null },
    event: { type: String, default: null },
    order: { type: String, enum: OrderStatus,  default: OrderStatus.PREPARING_ORDER },
    errorCode : { type: String, default: null },
  },
  {
    timestamps: true,
  },
);
