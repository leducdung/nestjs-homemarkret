import {
  Controller,
  Get,
  Request,
  Post,
  UseGuards,
  Body,
  Put,
  Param,
  Query,
  Delete,
  Res,
} from '@nestjs/common';
import { JwtAuthGuard } from '../../auth/jwt/jwt-auth.guard';
import {
  CreatePaymentDto,
  DataUpdatePaymentDto,
  ParamPaymentDto,
  FindPaymentDto,
  UpdateManyPaymentsData,
} from './model/payment.dto';
import { ApiTags } from '@nestjs/swagger';
import { PaymentService } from './payment.service';
import { PaymentCombinedService } from "./payment.combined.service";

@ApiTags('payment')
@Controller('')
export class PaymentController {
  constructor(
    private readonly paymentService: PaymentService,
    private readonly paymentCombinedService: PaymentCombinedService
    ) {}

  @UseGuards(JwtAuthGuard)
  @Get('payment')
  async findMany(
    @Query() query: FindPaymentDto,
    @Request() { req },
  ) {
    try {
      const payment = await this.paymentService.findMany({
        query,
      });

      return payment;
    } catch (error) {
      throw error;
    }
  }


  @UseGuards(JwtAuthGuard)
  @Get('payment/:paymentID')
  async findOne(
    @Param() { paymentID }: ParamPaymentDto,
    @Request() req,
  ) {
    try {
      return this.paymentService.findProduct({ _id: paymentID });
    } catch (error) {
      throw error;
    }
  }

  @UseGuards(JwtAuthGuard)
  @Post('payment')
  async createOne(
    @Body() createNoticationDto: CreatePaymentDto,
    @Request() req,
  ) {
    try {
      return this.paymentCombinedService.CreatePaymentHandle({
        data: {
          createdBy: req.user.resultUser._id,
          ...createNoticationDto,
        },
      });
    } catch (error) {
      throw error;
    }
  }

  @UseGuards(JwtAuthGuard)
  @Post('payment/direct_payment')
  async DIRECT_PAYMENT(
    @Body() createNoticationDto: CreatePaymentDto,
    @Request() req,
  ) {
    try {
      return this.paymentCombinedService.CreatePaymentHandleDirectPayment({
        data: {
          createdBy: req.user.resultUser._id,
          ...createNoticationDto,
        },
      });
    } catch (error) {
      throw error;
    }
  }

  @UseGuards(JwtAuthGuard)
  @Delete('payment/:paymentID')
  async Delete(@Param() paymentID: ParamPaymentDto
  , @Request() req
  ) {
    try {
      return this.paymentService.deleteOne({
        _id: paymentID.paymentID,
      });
    } catch (error) {
      throw error;
    }
  }

  @UseGuards(JwtAuthGuard)
  @Put('payment/:paymentID')
  async updateOne(
    @Param() paymentID: ParamPaymentDto,
    @Body() data: DataUpdatePaymentDto,

  ) {
    try {
      return this.paymentCombinedService.UpdateOneHandle({
        data,
        query: { _id: paymentID.paymentID },
      });


    } catch (error) {
      throw error;
    }
  }

  @Get('payment/momo/checkPayment')
  async checkPayment(
    @Query() query,
    @Res() res,
  ) {
    try {
      const payment = await this.paymentCombinedService.CheckPaymentHandle({
        query,
      });

      return res.redirect(`http://payment-data-hm.eu-4.evennode.com`)
    } catch (error) {
      throw error;
    }
  }

  @Get('payments/getArayProduct/data')
  async getArrayProduct(
    @Query() query: FindPaymentDto
    ) {
    try {
      const array = await this.paymentService.getArrayProduct({
        query,
      });

      return array;
    } catch (error) {
      throw error;
    }
  }

  @Put('payments/updateMany/data')
  async updateMany(
    @Body() data : UpdateManyPaymentsData,
    ) {
    try {
      const arrayPayment = await this.paymentService.updateMany({
        dataUpdate: data,
      });

      return arrayPayment;
    } catch (error) {
      throw error;
    }
  }


  @UseGuards(JwtAuthGuard)
  @Get('payment/aggregate/data/log')
  async findManyAggregate(
    @Query() query: FindPaymentDto,
    @Request() { req },
  ) {
    try {
      const payment = await this.paymentService.findManyAggregate({
        query,
      });

      return payment;
    } catch (error) {
      throw error;
    }
  }


  @UseGuards(JwtAuthGuard)
  @Put('payment/order/employee/:paymentID')
  async orderOne(
    @Param() paymentID: ParamPaymentDto,
    @Body() data : UpdateManyPaymentsData,
    @Request() req,
  ) {
    try {
      return this.paymentCombinedService.OrderOneHandle({
        data:{
          ...data,
          userOrder: req.user.resultUser._id,
        },
        query: { _id: paymentID.paymentID },
      });


    } catch (error) {
      throw error;
    }
  }

}
